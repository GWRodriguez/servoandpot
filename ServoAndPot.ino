#include <Servo.h>


Servo servo;
const int pinPot = A0;
int pot = 0;
int angle = 0;


void setup() {
  servo.attach(9);
  Serial.begin(9600);
}


void loop() {
  pot = analogRead(pinPot);
  Serial.print("Pot value: ");
  Serial.print(pot);

  angle = map(pot, 0, 1023, 0, 179);
  Serial.print(" Angle: ");
  Serial.println(angle);

  servo.write(angle);
  delay(15);
}
